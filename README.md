# Online sales assistant automation for service-based products
----
Created by: Benedek Molnár | molnaben | molnaben@fit.cvut.cz
<br>
Supervisor: Ing. Jiří Hunka | jiri.hunka@fit.cvut.cz
<br>
Front end: https://gitlab.fit.cvut.cz/molnaben/aibraham-thesis-project
<br>
Back end: https://gitlab.fit.cvut.cz/molnaben/aibraham-thesis-project-backend

### Assignment
----
The aim of this work is to implement an online assistance system that will support the owners of service products. The focus is on providing expansion and growth opportunities for service product owners and the ability to automate initial communication between service owners and their potential customers.
Proceed with the following steps:
1. Conduct a thorough analysis of the sales process and customer acquisition issues for service products, including the capabilities and the process of the initial communication between the service operator and the service consumer.
2. Analyse the possibilities of automating the initial communication process.
3. Based on the analysis, propose possible solutions.
4. Select a service product provider and implement a prototype solution according to the selected options.
5. Design appropriate test suites and perform thorough testing.
6. Evaluate the benefits and prospects of your solution, suggest possible improvements for the future.

### Abstract
----
The goal of this thesis was to analyze and evaluate the sales process and the currently available tools for assisting it. The aim was to find a possible solution for automating the initial contact with potential clients to reduce repetitive manual labor. By studying the current possibilities and market trends, I designed an application for creating virtual agents using OpenAI's GPT language models. These agents are easily integrable and human-like assistants capable of assisting any size of business seeking help with sales or customer support. After thorough analysis and design, I successfully implemented both the front-end and back-end of the application and subjected it to comprehensive testing to evaluate the concept and the quality of the application

#### Setup
----

1. **Install Node.js and Git**: If you haven't already, download and install Node.js and Git on your Windows machine.

2. **Install MySQL**: Download and install MySQL from the official website (https://dev.mysql.com/downloads/mysql/). Follow the installation instructions provided.

3. **Clone the Repositories**: Use Git to clone the repositories of both the front end and the back end to your local machine.

4. **Install Dependencies**: Navigate to the directories of both projects using the command line. Then, run `npm install` in each directory to install the required dependencies specified in the `package.json` files.

5. **Set Up MySQL Database**:
   - Create a new MySQL database using your preferred MySQL client or command line tools.
   - Use the structure found in the text document in the back-end folder to create the necessary tables and schema in your newly created database.

6. **Configure the Back End**:
   - Navigate to the back end directory.
   - Open the `.env` file in a text editor.
   - Fill in the missing details such as database connection details, OpenAI key and other required keys in the `.env` file.
   - Save the `.env` file.
   - You might need to configure the CORS setup in the file `authroutes.js`.

7. **Configure the Front End**:
   - Set up the Axios baseroute for the API in `App.jsx`.

8. **Start the Back End**: In the directory of the Node.js back end, use the command line to start the server. This might be done by running `npm start`.

9. **Start the Front End**: In the directory of the React front end, also using the command line, start the development server. This is usually done by running `npm start`.

10. **Access Your Application**: Once both the back end and front end servers are running, you should be able to access your application by opening a web browser and navigating to the appropriate URL. Typically, for React applications, this will be something like `http://localhost:3000`.

#### Future development
----
In the source folder of the back-end I added the sources for a Trello board, which you might use to track the analysis or development for the recommended features described in my thesis. In case of interest, import the sourcefile in Trello (https://trello.com).