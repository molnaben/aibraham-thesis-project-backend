const Agent = require('../models/agent')
const { createAssistant } = require('../integrations/openai');
const { generateSecretKey } = require('../helpers/secretKey');
const { getAll, getLastSevenDays } = require('../models/session');
const { updateAgentKnowledge } = require('./knowledgeController');

/**
 * FETCH ALL AGENTS FOR THE COMPANY
 * @param {*} req.company Company ID provided by the middleware
 * @returns Agents list
 */
const getAgentsForCompany = async (req, res) => {
    try {
        const companyId = Number(req.company); //Received form middleware
        const agents = await Agent.getAll(companyId);
        return res.json(agents);
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * GET HOME INFORMATION FOR THE AGENT
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.params.id Agent ID
 * @returns JSON data object: {
            total_session: sessionCount,
            total_tickets: ticketCount,
            graph: sevenDays
        }
 */
const getAgentHome = async (req, res) => {
    try {
        //Get agent
        const companyId = Number(req.company);
        const agent = await Agent.getAgent(Number(req.params.id));

        //Check validity of request - agent belongs to company
        if (companyId !== Number(agent.company_id))
            return res.status(403).json({ error: 'You company does not own the agent.' })

        //Get session and Ticketcound
        const sessions = await getAll(agent.id_agent);

        const sessionCount = sessions.filter((item)=>{return ! Boolean(item.is_ticket)}).length;
        const ticketCount = sessions.filter((item)=>{return Boolean(item.is_ticket)}).length;

        //Get graph data
        const sevenDays = await getLastSevenDays(agent.id_agent);

        return res.json({
            total_session: sessionCount,
            total_tickets: ticketCount,
            graph: sevenDays
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * GET DETAILS ABOUT SINGLE AGENT
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.params.id Agent ID
 * @returns Agent details or 403 if the company doesnt own the agent
 */
const getAgentDetails = async (req, res) => {
    try {
        //Fetch agent
        const companyId = Number(req.company);
        const agent = await Agent.getAgent(Number(req.params.id));

        //Check company validity
        if (companyId !== Number(agent.company_id))
            return res.status(403).json({ error: 'You company does not own the agent.' })

        return res.json(agent)
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * UPDATE AGENT SETTINGS
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.body.accept_email Agents ability to accept emails
 * @param {*} req.body.error_message Custom error message
 * @param {*} req.body.id_agent ID of the agent
 * @param {*} req.body.is_custom_error Use custom error message?
 * @param {*} req.body.is_active Status of the agent
 * @returns Database update response
 */
const updateAgentSettings = async (req, res) => {
    try {

        const { accept_email, error_message, id_agent, is_custom_error, is_active } = req.body
        const requestCompany = Number(req.company);
        
        if (!id_agent) {
            return res.status(404).json({ error: 'No agent selected.' })
        }

        //Fetch agent
        const origSettings = await Agent.getAgent(id_agent);

        //Check company validity
        if (Number(origSettings.company_id) !== requestCompany) {
            return res.status(403).json({ error: 'This agent doesnt belong to you.' })
        }

        //Update settings
        const updatedAgent = await Agent.updateAgent(id_agent, origSettings.name, requestCompany, is_active, is_custom_error, error_message, accept_email);
        return res.json(updatedAgent);
    } catch (error) {
        console.log(error);
        return res.status(500);
    }
}

/**
 * ADD NEW AGENT
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.body.name Name of the agent
 * @param {*} req.body.type Type of the agent
 * @returns Success status and Agent ID
 */
const addAgentForCompany = async (req, res) => {
    try {
        const { name, type } = req.body
        const requestCompany = Number(req.company);

        //Initial configuration
        var baseInstruction = '';
        const status = 1;
        const is_custom_error = 0;
        const error_message = '';
        const accept_email = 0;

        //Default behaviour
        if( Number(type) === 0)
            baseInstruction = process.env.AGENT_SALES_TEXT;
        else 
            baseInstruction = process.env.AGENT_SUPPORT_TEXT;

        //Create assistant at Openai
        const assistant = await createAssistant(name, baseInstruction)

        if (!assistant.id)
            return res.status(503).json({ error: "External service unavailable." })

        //Assign secret key
        const secretKey = generateSecretKey();

        //Register agent
        const createdAgent = await Agent.createAgent(name, requestCompany, status, is_custom_error, error_message, accept_email, assistant.id, type, secretKey );
        
        //Load initial configuration
        if(createdAgent && createdAgent.insertId)
            await updateAgentKnowledge(createdAgent.insertId);

        return res.json({success: true, agentId: createdAgent.insertId});

    } catch (error) {
        console.log(error);
        return res.status(500);
    }
}

module.exports = {
    getAgentsForCompany,
    getAgentDetails,
    updateAgentSettings,
    addAgentForCompany,
    getAgentHome
}