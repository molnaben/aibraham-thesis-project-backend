const User = require('../models/user')
const {comparePassword} = require('../helpers/auth')
const jwt = require('jsonwebtoken');
const ROLES_LIST = require('../models/roles_list');

/**
 * LOG INTO THE APPLICATION
 * @param {*} req.body.email Users email
 * @param {*} req.body.password Users password
 * @returns Users details and auth token, or success status and error message
 */
const loginUser = async (req, res) => {
    try {
        const {email, password} = req.body;

        //Check if user exists
        const user = await User.findOne(email);
        if(!user || !user.id){
            return res.json({success: false, error: 'No user found.'})
        }

        //Check if passwords match
        const match = await comparePassword(password, user.password);
        if(match){

            //Assign roles
            let roles = []
            if(user.is_admin) roles.push(ROLES_LIST.Admin)
            if(user.is_supervisor) roles.push(ROLES_LIST.Supervisor)
            if(user.is_manager) roles.push(ROLES_LIST.Manager)

            //Log in and send token
            jwt.sign({email: user.email, id: user.id, name: user.name, roles: roles, company: user.company_id}, process.env.JWT_SECRET, {expiresIn: '1d'}, (err, iToken) => {
                if(err) throw err;
                res.json({id: user.id, name: user.name, email: user.email, roles: roles, company: user.company_id, token: iToken})
            })
        }else{
            res.json({success: false, error: 'Incorrect credentials.'})
        }
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}

module.exports = {
    loginUser,
}