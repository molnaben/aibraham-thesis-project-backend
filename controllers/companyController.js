const { hashPassword } = require('../helpers/auth');
const Company = require('../models/company');
const { findOne } = require('../models/user');

/**
 * REGISTER NEW COMPANY
 * @param {*} req.body.businessName Business name
 * @param {*} req.body.businessId Business ID
 * @param {*} req.body.vatiId Business VAT ID
 * @param {*} req.body.isBusinessCustomer Is business?
 * @param {*} req.body.name Supervisor name
 * @param {*} req.body.email Sueprvisor email
 * @param {*} req.body.password Supervisor password
 * @returns success status and error message
 */
const registerCompany = async (req, res) => {
    try {

        const { businessName, businessId, vatiId, isBusinessCustomer, name, email, password } = req.body;
        const isBusiness = Number(isBusinessCustomer)

        // Check if name was entered
        if (!businessName) {
            return res.json({ success: false, error: 'Organisation name is required.', type: 1 })
        };

        // Check if it is business then needs businessID
        if (!businessId && isBusiness) {
            return res.json({ success: false, error: 'Business identification number is required.', type: 2 })
        };

        // Check if supervisor name was entered
        if (!name)
            return res.json({ success: false, error: 'Missing name.', type: 3 })

        // Check if email was entered
        if (!email)
            return res.json({ success: false, error: 'Missing email adddress.', type: 4 })

        // Check if password was entered
        if (!password)
            return res.json({ success: false, error: 'Missing password.', type: 5 })

        // Check if password length is atleast 6 chars
        if (password.length < 6)
            return res.json({ success: false, error: 'Password is too short. (min. 5 char)', type: 6 })

        // Check if user email exists
        const userExists = await findOne(email);

        if( userExists && userExists.id ){
            return res.json({ success: false, error: 'User with this email already exists.', type: 7 })
        }

        // Hash the new password
        const passwordHashed = await hashPassword(password);

        // Register company and supervisor
        const registered = await Company.register(businessName, isBusiness, businessId, vatiId, name, email, passwordHashed)
   
        // Control registration
        if(!registered.success)
            return res.json({success: false, error: "Company could not be registered. Try again later."})

        return res.json({success: true}); //res.json(company);
    } catch (error) {
        console.log(error);
        return { error: "Unsuccessful company registration." }
    }
}


/**
 * UPDATE COMPANY BILLING DETAILS
 * Expects verifyRoles and verifyToken middlewares
 * @param {*} req.body.name Business name
 * @param {*} req.body.business_id Business ID
 * @param {*} req.body.vat_id VAT ID
 * @param {*} req.body.is_business Is business?
 * @param {*} req.company Company ID provided by the middleware
 * @returns update response
 */
const updateCompany = async (req, res) => {
    try {
        const { name, business_id, vat_id, is_business } = req.body;
        const isBusiness = Number(is_business)
        const company_id = req.company;

        //Get company
        const company = await Company.findOneById(company_id);

        //Update company
        const updatedCompany = await Company.update(company_id, name, isBusiness, business_id, vat_id, company.is_active, company.membershipend);
        
        return res.json(updatedCompany)
    } catch (error) {
        console.log(error);
        return res.status(500);
    }

}

/**
 * INCREASE MONTHLY TOKEN COST AFTER ACTION
 * @param {*} companyId Company who did the action
 * @param {*} additionalCost Added token cost
 * @returns success status
 */
const increaseCost = async (companyId, additionalCost) => {
    return await Company.addToCost(companyId, additionalCost);
}

/**
 * RESET COMPANY'S MONTHLY TOKEN COST
 * @param {*} companyId Company ID
 * @returns success status
 */
const resetCost = async (companyId) => {
    return await Company.resetCost(companyId)
}

module.exports = {
    registerCompany,
    updateCompany,
    registerCompany,
    increaseCost,
    resetCost
}