const Invoicer = require('../integrations/invoice')
const Company = require('../models/company')
const Bill = require('../models/bill')
const cron = require('node-cron');

/**
 * UNIVERSAL CREATE NEW INVOICE
 * @param {*} companyId Creator company ID
 * @param {*} isPaid Is the invoice paid?
 * @param {*} title Invoice title
 * @param {*} products Invoice products in the form: [{"name":"", "unit_price": 0}]
 * @returns success status, invoice id, invoice token: { success: true, id: invoice.id, token: invoice.token }
 */
const createInvoice = async (companyId, isPaid, title, products) => {
    try {

        //Find invoicer company
        const company = await Company.findOneById(companyId)
        if (!company.id)
            throw "Invoicer - company not found."

        //Create invoice with invoicer
        const invoice = await Invoicer.createInvoice(
            title,
            products,//[{"name":"Aibraham Membership","unit_price": process.env.MEMBERSHIP_PRICE}],
            company.name,
            company.business_id,
            company.vat_id
        )

        //Check invoice status
        if (!invoice.success)
            return { success: false, error: invoice.error_message }

        //Register in this system
        const bill = await Bill.createBillForCompany(invoice.id, invoice.token, isPaid, company.id, invoice.amount);

        //Check insertion
        if (!bill.insertId)
            throw "Invoicer - bill generated but could not be saved into the database."

        return { success: true, id: invoice.id, token: invoice.token }

    } catch (error) {
        console.log(error)
        return { success: false, error: "Invoicer server error." }
    }
}

/**
 * LIST ALL INVOICES FOR COMPANY
 * @param {*} req.company Company ID provided by the middleware
 * @returns list of invoices
 */
const getInvoicesForCompany = async (req, res) => {
    try {
        const companyId = req.company;
        const invoices = await Bill.getAllForCompany(companyId);
        return res.json(invoices.reverse())
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * CONTROL ALL BILLS IN THE DATABASE
 * If the bill is due validity date, disable company
 */
const checkBills = async () => {
    try {
        //Get all bills
        const bills = await Bill.getBills();

        const currentDate = (new Date())

        //Iterate all bills
        for (let index = 0; index < bills.length; index++) {
            const bill = bills[index];

            const dueDate = new Date(bill.due_date);

            //If bill is due, disable company
            if (currentDate > dueDate && !Number(bill.is_paid)) {
                const result = await Company.disableCompany(bill.company_id)
                if (result.success)
                    console.log(`Bill ${bill.id_bill} is not paid. Disabling company.`);
            }
        }

        return { success: true }
    } catch (error) {
        console.log(error)
        return {success:false}
    }

}

/**
 * BILL MONTHLY OUTSTANDING TOKENS
 */
const billOutstanding = async () => {
    try {
        //Get all companies
        const companies = await Company.getCompanies();

        //Iterate all companies
        for (let index = 0; index < companies.length; index++) {
            const company = companies[index];
            
            //Count tokens to real currency
            const cost = company.monthly_cost * process.env.BILLING_TOKEN_COST;

            //If the cost is more than minimum, create bill and reset cost
            if(company.monthly_cost > Number(process.env.BILLING_COST_MINIMUM)){
                const invoice = await createInvoice(company.id, 0, "Aibraham tokens", [{"name":"Aibraham used tokens","unit_price": cost}])
            
                //Not billed minimum doesnt get reset
                if( invoice.success ){
                    const result = await Company.resetCost(company.id)
                    if(result.success) console.log("Company", company.id, "token reset.")
                }

            }
        }
    } catch (error) {
        console.log(error)
        return {success:false}
    }
}

/**
 * GET BILLING INFORMATION FOR BILLING PAGE
 * @param {*} req.company Company ID provided by the middleware
 * @returns JSON object or 404 if company is not found
 */
const getBillingInfo = async (req, res) =>{

    //Generate string date with the 1st day of the next month
    function getNextFirstDayOfMonth() {
        
        const currentDate = new Date();
    
        // Calculate next month and handle December
        let nextMonth;
        if (currentDate.getMonth() === 11) {
            nextMonth = new Date(currentDate.getFullYear() + 1, 0, 1); // Next year, January
        } else {
            nextMonth = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1); // Next month
        }
    
        return nextMonth;
    }

    //Get next month 1st date
    const nextDate = getNextFirstDayOfMonth();
    const nextDateBlock = new Date (nextDate)
    
    //Set 14 day interval for validity
    nextDateBlock.setDate(nextDateBlock.getDate() + 14)

    try {

        //Fetch company details
        const companyId = Number(req.company);
        const company = await Company.findOneById(companyId);

        if(!company.id){
            return res.status(404).json({error:'No company found.'})
        }

        //Return formatted data
        return res.json({
            account: {
                status: company.is_active,
                activated: '---',
                valid: company.membership_end
            },
            balance: {
                outstanding: company.monthly_cost * process.env.BILLING_TOKEN_COST,
                turnover: company.monthly_cost,
                next: nextDate,
                block: nextDateBlock
            }
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * CHECK IF THE COMPANIES HAVE VALID MEMBERSHIP
 */
const checkMemberships = async () => {
    try {
        //Get all companies
        const companies = await Company.getCompanies();

        //Iterate all companies
        for (let index = 0; index < companies.length; index++) {
            const company = companies[index];
            
            const currentDate = new Date();

            //Check if their membership is still valid, if not, disable company
            if( currentDate > company.membership_end){
                const result = await Company.disableCompany(company.id);
                if(result.success)
                    console.log("Company", company.id, "disabled - no membership.")
                else
                    console.log("Company", company.id, "could not be disabled but has no membership.")
            }
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * PAY MEMBERSHIP AND ACTIVATE ACCOUNT
 * @param {*} req.company Company ID provided by the middleware
 * @returns success status
 */
const payMembership = async (req, res) => {
    try {
        
        const companyId = Number(req.company);

        //Add memebership length to the account status
        const result = await Company.addToMembership(companyId, Number(process.env.MEMBESHIP_DAYS));

        //Check success
        if(!result.success){
            return res.json(result);
        }

        //If success activate account
        const activateResult = await Company.activateCompany(companyId);

        //Generate invoice for the paid amount
        const invoiceResult = await createInvoice(companyId, 1,"Aibraham membership", [{"name":"Aibraham 30 day membership","unit_price": process.env.MEMBERSHIP_PRICE}])
            
        return res.json(activateResult)
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * PAY INVOICE DUMMY FUNCTION
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.params.id Payable invoice id
 * @returns success status
 */
const payInvoice = async (req, res) => {
    try {
        
        const companyId = Number(req.company);
        const invoicId = Number(req.params.id);

        //Add memebership length to the account status
        const result = await Bill.markPaid(invoicId);

        //If success activate account
        const activateResult = await Company.activateCompany(companyId);

        return res.json(activateResult)
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

module.exports = {
    createInvoice,
    getInvoicesForCompany,
    billOutstanding,
    getBillingInfo,
    payMembership,
    payInvoice
}


/** The first field 0 represents the minute (0 to 59).
The second field 0 represents the hour (0 to 23).
The third field 1 represents the day of the month (1 to 31).
The fourth field * represents the month (1 to 12).
The fifth field * represents the day of the week (0 to 6, where 0 represents Sunday). */


cron.schedule('0 1 * * *', checkMemberships); // every day at 1 check if they have valid membership
cron.schedule('0 2 * * *', checkBills); // every day at 2 check if they have outstanding bills
cron.schedule('0 0 1 * *', billOutstanding); // every month on day 1 bill outstanding tokens

console.log("Checking membership at 01:00 each day - Activated.")
console.log("Checking bills at 02:00 each day- Activated.")
console.log("Billing tokens at 00:00 each month - Activated.")
