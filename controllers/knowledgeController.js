const Knowledge = require('../models/knowledge')
const Agent = require('../models/agent');
const AGENT_DEFAULT_FUNCTIONS = require('../models/agent_functions');
const { updateAssistant } = require('../integrations/openai');

/**
 * GET KNOWLEDGE LIST FOR AGENT
 * @param {*} req.params.id Agent ID
 * @returns list of knowledge
 */
const getKnowledgeForAgent = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const knowledge = await Knowledge.getAll(agentId);
        return res.json(knowledge);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * GET DETAILS OF A SINGLE KNOWLEDGE
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.kId Knowledge ID
 * @returns knowledge data or 403 if doens't have access
 */
const getOneKnowledge = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const knowledgeId = Number(req.params.kId);

        //Fetch and validate knowledge
        const knowledge = await Knowledge.getOne(knowledgeId);
        if(!knowledge){
            return res.status(404).json({ error: 'Knowledge doesnt exist.' });
        }
        if (knowledge.agent_id !== agentId) {
            return res.status(403).json({ error: 'Knowledge doesnt belong to your agent.' });
        }

        return res.json(knowledge);

    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * UPDATE KNOWLEDGE DETAILS
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.kId Knowledge ID
 * @param {*} req.body.title Knowledge title
 * @param {*} req.body.description Knowledge description
 * @param {*} req.body.status Knowledge status
 * @param {*} req.body.isFunction Is informational or functional
 * @returns update response
 */
const updateKnowledge = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const knowledgeId = Number(req.params.kId);
        const { title, description, status, isFunction } = req.body;

        //Fetch and validate knowledge
        const knowledge = await Knowledge.getOne(knowledgeId);
        if(!knowledge){
            return res.status(404).json({ error: 'Knowledge doesnt exist.' });
        }
        if (knowledge.agent_id !== agentId) {
            return res.status(403).json({ error: 'Knowledge doesnt belong to your agent.' });
        }

        //Update knowledge
        const updatedKnowledge = await Knowledge.updateKnowledge(knowledgeId, title, description, status, isFunction);

        //Update agents knowledge based on the new knowledge
        await updateAgentKnowledge(agentId);

        return res.json(updatedKnowledge);

    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * ADD NEW KNOWLEDGE TO AGENT
 * @param {*} req.params.id Agent ID
 * @param {*} req.body.title Knowledge title
 * @param {*} req.body.description Knowledge description
 * @param {*} req.body.status Knowledge status
 * @param {*} req.body.isFunction Is informational or functional
 * @returns insert status
 */
const createKnowledgeForAgent = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const { title, description, status, isFunction } = req.body;

        //Register new knowledge
        const knowledge = await Knowledge.createKnowledgeForAgent(agentId, title, description, status, isFunction);

        //Update agents knowledge with the new knowledge
        if(status) updateAgentKnowledge(agentId)

        return res.json(knowledge);
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * UPDATE AGENT KNOWLEDGE
 * Agents gets recinfigured with the knowledge defined by the user.
 * @param {*} agentId Agents ID
 * @returns 
 */
const updateAgentKnowledge = async (agentId) => {

    try {

        //Get all knowledge defined for the agent
        const knowledge = await Knowledge.getAll(agentId);

        //Get agents configuration
        const agent = await Agent.getAgent(agentId);

        //Filter knowledge to functional and informational
        const infos = knowledge.filter(knowledge => { return ( !Number(knowledge.is_function) && Number(knowledge.is_active)) })
        const functions = knowledge.filter(knowledge => { return (Number(knowledge.is_function) && Number(knowledge.is_active)) })

        //Result containers
        var collectedInfos = '';
        var collectedFunctions = [];

        //Add default behaviour
        if (Number(agent.type) === 0)
            collectedInfos += process.env.AGENT_SALES_TEXT;
        if (Number(agent.type) === 1)
            collectedInfos += process.env.AGENT_SUPPORT_TEXT;

        collectedInfos += '\n';

        //Collect informational knowledge
        for (let index = 0; index < infos.length; index++) {
            const info = infos[index];

            collectedInfos += info.title + ':\n';
            collectedInfos += info.content + '\n\n';
        }

        //Add default functions
        collectedFunctions = collectedFunctions.concat(AGENT_DEFAULT_FUNCTIONS)

        //Collect functional knowledge
        for (let index = 0; index < functions.length; index++) {
            const func = functions[index];

            collectedFunctions = collectedFunctions.concat(JSON.parse(func.content));
        }

        //Reconfigure Openai assistant
        const result = await updateAssistant(agent.assistant_id, collectedInfos, collectedFunctions, agent.name);

        return {success:true, result}
    } catch (error) {
        console.log(error)
        return {success:false, error: 'Internal error.' }
    }
}

/**
 * DELETE EXISTING KNOWLEDGE
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.kId Knowledge ID
 * @returns delete result
 */
const deleteKnowledge = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const knowledgeId = Number(req.params.kId);

        //Validate knowledge
        const knowledge = await Knowledge.getOne(knowledgeId);
        if(!knowledge){
            return res.json({ success: true }); //If it doesnt exist we dont have to delete
        }
        if (knowledge.agent_id !== agentId) {
            return res.status(403).json({ error: 'Knowledge doesnt belong to your agent.' });
        }

        //Deletee knowledge
        const result = await Knowledge.removeKnowledge(knowledgeId);

        return res.json(result)
    } catch (error) {
        console.log(error)
        return {success:false, error: 'Internal error.' }
    }
}

module.exports = {
    getKnowledgeForAgent,
    createKnowledgeForAgent,
    updateAgentKnowledge,
    getOneKnowledge,
    updateKnowledge,
    deleteKnowledge
}
