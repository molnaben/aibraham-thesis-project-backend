const { listMessagesOnThread, addMessageToThread, runThread, retrieveRun, returnLastRun, submitToolOutput } = require('../integrations/openai');
const Session = require('../models/session');
const { createSession } = require('./sessionController');
const Agent = require('../models/agent');
const { increaseCost } = require('./companyController');
const { getMessagesForTicket, addMessageToTicket } = require('../models/message');

/**
 * FETCH CONVERSATION ON A PUBLIC DOMAIN
 * @param {*} req.params.id Session ID
 * @returns Json object { isTicket: session.is_ticket, messages: messages }
 */
const fetchPublicSession = async (req, res) => {

    try {
        const { id } = req.params

        //Get session details
        const session = await Session.getOne(id);

        //If its ticket
        if (Boolean(session.is_ticket)) {

            //Fetch messages from local source
            const messagesRaw = await getMessagesForTicket(session.id_session);

            //Put them into correct form
            const messages = messagesRaw.map(messageItem => {
                return {
                    type: 'message',
                    role: messageItem.sender,
                    content: messageItem.message
                }
            })
            return res.json({ isTicket: session.is_ticket, messages: messages })
        }

        //Get thread status
        const lastRun = await returnLastRun(session.thread_id);

        //If thread is wairing for answer
        if (lastRun && lastRun.status === "requires_action") {

            //Get called tools
            const tool_calls = lastRun.required_action.submit_tool_outputs.tool_calls;

            //Format into readable form
            const extendedCalls = tool_calls.map(tool => {
                return {
                        ...tool,
                        threadId: lastRun.thread_id,
                        runId: lastRun.id
                }
            })

            //Return as message for processing
            return res.json({isTicket: session.is_ticket, messages: extendedCalls});
        }

        //If no action, fetch the messages on the thread
        const messageList = await listMessagesOnThread(session.thread_id)

        //Format them
        const messages = messageList.data.map(messageItem => {
            return {
                type: 'message',
                role: messageItem.role,
                content: messageItem.content[0].text.value
            }
        })

        session.messages = messages.reverse();

        return res.json({ isTicket: session.is_ticket, messages: session.messages });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }

}

/**
 * SUBMIT CUSTOM FUNCTION OUTPUT TO WAITING THREAD
 * @param {*} req.body.threadId Thread ID
 * @param {*} req.body.runId Run ID
 * @param {*} req.body.output Custom function output
 * @returns 
 */
const postPublicFunctionOutput = async (req, res) => {
    try {
        const { threadId, runId, output } = req.body

        //Submit output
        const response = await submitToolOutput(threadId, runId, output);

        //const session = await Session.getOneByThread(threadId)
        //req.params.id = session.id_session

        return res.json({ success: true })
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * REGISTER A NEW SESSION FROM THE PUBLIC DOMAIN
 * @param {*} req.body.agentKey Agents secret key 
 * @param {*} req.body.newMessage Initial user message
 * @returns 
 */
const createPublicSession = async (req, res) => {
    try {

        const { agentKey, newMessage } = req.body

        //Set up to be processed by a different function
        req.params.id = agentKey;
        req.body.messages = [{ content: newMessage, role: "user", type: "message" }]

        //Create session internally
        const sess = await createSession(req)

        //Validate creation
        if (!sess.success)
            return res.json(sess)

        //Setup for handling initial message
        req.params.id = sess.session.insertId

        // finishes the task by adding the message and running it
        return addPublicMessage(req, res) 


    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * ADD MESSAGE TO SESSION FROM PUBLIC DOMAIN
 * @param {*} req.params.id Session ID
 * @param {*} req.body.newMessage Initial user message
 * @returns 
 */
const addPublicMessage = async (req, res) => {

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    try {
        const sessionId = Number(req.params.id)
        const message = req.body.newMessage
        const role = "user";

        //Get session details
        const session = await Session.getOne(sessionId);

        //Validate session
        if (!session)
            return res.status(404).json({error: "No session found."})

        if (!session.id_session)
            return res.status(403).json({ error: "Session is not available." });

        //If session is ticket
        if (Boolean(session.is_ticket)) {

            //Insert message to local storage
            const result = await addMessageToTicket(role, message, sessionId);
            if (!result || !result.insertId)
                return res.json({ success: false, error: "Message could not be added to the ticket." })

            return res.json({ success: true, sessionId: session.public_key })
        }

        //If not ticket, get agent details
        const agent = await Agent.getAgent(session.agent_id)

        //Validate agent
        if (!agent.assistant_id)
            return res.status(403).json({ error: "Agent is not available." });

        //Post and validate insertion on Thread
        const creation = await addMessageToThread(session.thread_id, role, message);
        if (!creation.id)
            return res.status(503).json({ error: "External service unavailable." })

        //Run thread on the assistant
        const run = await runThread(session.thread_id, agent.assistant_id);

        if (!run.id)
            return res.status(503).json({ error: "Thread could not be run." });

        //Return success for user
        res.json({ success: true, sessionId: session.public_key })

        //HOWEVER USED TOKENS NEEDS TO BE ADDED
        try {
            var result = {};

            //Check until run is complete 
            do {

                result = await retrieveRun(
                    session.thread_id,
                    run.id
                );

                if (result.status !== 'completed') {
                    await sleep(1000); // Wait for 1 second before next iteration
                }

                console.log("++", result.status)
            } while (result.status === 'in_progress' || result.status === 'requires_action')

            //If run is complete, fetch token count and increase the company token use and session cost
            if (result.status === 'completed') {
                const response = await increaseCost(agent.company_id, Number(result.usage.total_tokens))
                await Session.addToSessionCost(session.id_session, Number(result.usage.total_tokens))

                if (!response.success)
                    console.log("Message could not be invoiced.")
                else
                    console.log("Message invoice = ", result.usage.total_tokens, " tokens")
            }
        } catch (error) {
            console.log(error)
        }

        return;
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

module.exports = {
    fetchPublicSession,
    createPublicSession,
    addPublicMessage,
    postPublicFunctionOutput
}