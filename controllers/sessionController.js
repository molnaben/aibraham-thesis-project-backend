const { getUniqueid } = require('../helpers/uniqueId');
const { createThread, listMessagesOnThread } = require('../integrations/openai');
const Message = require('../models/message');
const Session = require('../models/session');

/**
 * FETCH ALL SESSIONS FOR AGENT
 * @param {*} req.params.id Agent ID
 * @returns list of all sessions
 */
const getSessionsForAgent = async (req, res) => {
    try {
        const agentId = Number(req.params.id);

        //Fetch all sessions
        const sessions = await Session.getAll(agentId);

        //Convert token cost to currency cost
        const costedSessions = sessions.map(session => {
            return {
                ...session,
                cost: session.total_cost * Number(process.env.BILLING_TOKEN_COST)
            }
        })
        return res.json(costedSessions.reverse());
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * FETCH ALL TICKETS FOR AGENT
 * @param {*} req.params.id Agent ID
 * @returns list of all tickets
 */
const getTicketsForAgent = async (req, res) => {
    try {
        const agentId = Number(req.params.id);

        //Fetch all tickets
        const tickets = await Session.getTickets(agentId);

        return res.json(tickets.reverse());
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * GET ALL THE SESSIONS SCHEDULED FOR FINE TUNE
 * @param {*} req.params.id Session ID
 * @returns list of sessions
 */
const getScheduledForAgent = async (req, res) => {
    try {
        const agentId = Number(req.params.id);

        const sessions = await Session.getScheduled(agentId);

        return res.json(sessions.reverse());
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * CREATE NEW SESSION
 * @param {*} req.params.id Agent ID
 * @param {*} req.body.tags Session tags
 * @param {*} req.body.is_email Is the session email communication
 * @param {*} req.body.customer_email Email of the customer
 * @param {*} req.body.is_ticket Is thee session ticket?
 * @returns success status and error message and session
 */
const createSession = async (req) => {
    try {
        const agentId = Number(req.params.id);
        const { tags, is_email, customer_email, is_ticket } = req.body;

        //INITIAL CONFIG
        const totalCost = 0;
        const isEmail = is_email ? is_email : 0;
        const customerEmail = customer_email ? customer_email : '';
        const isTicket = is_ticket ? is_ticket : 0;
        const finetune = 0
        const tagsList = tags ? tags : [{
            "name": "Open",
            "color": "#4CBB17"
        }]

        //Create new Openai thread
        const thread = await createThread();

        if (!thread.id)
            return { success: false, error: "External service unavailable." };

        //Generate public ID
        const publicKey = getUniqueid();

        //Register session locally
        const session = await Session.createSessionForAgent(agentId, totalCost, isEmail, customerEmail, tagsList, isTicket, thread.id, finetune, publicKey);

        return { success: true, session: session };
    } catch (error) {
        console.log(error);
        return { success: false, error: 'Internal error.' }
    }
}
/**
 * REGISTER NEW SESSION FROM INTERNAL DOMAIN
 * @returns success status and session or error message
 */
const addSessionToAgent = async (req, res) => {
    try {

        //Propagate creation
        const sess = await createSession(req);

        if (!sess.success)
            return res.json(sess)

        const session = await Session.getOne(sess.session.insertId);

        return res.json({ success: true, session: session });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * UPDATE TAGS ON SESSION
 * @param {*} req.body.tags New tags
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.sId Session ID
 * @returns success state or error
 */
const updateSessionTags = async (req, res) => {
    try {
        const { tags } = req.body;
        const { id, sId } = req.params;

        //Get session
        const session = await Session.getOne(sId);

        //Verify session
        if (!session)
            return res.status(404).json({ error: "Session doesnt exist." });

        if (session.agent_id !== Number(id))
            return res.status(403).json({ error: "Session doesnt belong to your agent." });

        //Update session
        const sessionNew = await Session.updateSession(sId, session.total_cost, JSON.stringify(tags), session.is_ticket, session.finetune);

        return res.json({ success: true });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' })
    }

}

/**
 * FETCH SINGLE SESSION DATA
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.sId Session ID
 * @returns single session
 */
const getSessionData = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const sessionId = Number(req.params.sId);

        //Fetch session
        const session = await Session.getOne(sessionId);

        //Verify session
        if (!session)
            return res.status(404).json({ error: "Session doesnt exist." });

        if (session.agent_id !== agentId) {
            return res.status(403).json({ error: 'Session doesnt belong to your agent.' });
        }

        //if session is ticket 
        if (Boolean(session.is_ticket)) {

            //Get messages from local storage
            const messagesRaw = await Message.getMessagesForTicket(session.id_session);

            //Format output
            const messages = messagesRaw.map(messageItem => {
                return {
                    type: 'message',
                    role: messageItem.sender,
                    content: messageItem.message
                }
            })
            session.messages = messages
            return res.json(session)
        }

        //If not ticket, fetch messages from assistant
        const messageList = await listMessagesOnThread(session.thread_id)

        //Format data
        const messages = messageList.data.map(messageItem => {
            return {
                role: messageItem.role,
                content: messageItem.content[0].text.value
            }
        })

        session.messages = messages.reverse();

        return res.json(session);

    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * REMOVE SESSIONS
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.sId Session ID
 * @returns remove status
 */
const removeSession = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const sessionId = Number(req.params.sId);

        //Fetch session
        const session = await Session.getOne(sessionId);

        //Verify session
        if (!session)
            return res.json({ success: true }); //If it doesnt exist i dont have to delete it
        if (session.agent_id !== agentId) {
            return res.status(403).json({ error: 'Session doesnt belong to your agent.' });
        }

        //Remove session
        const response = await Session.removeSession(sessionId);
        return res.json(response)
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * ASSIGN SESSION TO TICKET
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.sId Session ID
 * @returns success state and data, or error
 */
const assignAsTicket = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const sessionId = Number(req.params.sId);

        //Get session
        const session = await Session.getOne(sessionId);

        //Verify session
        if (!session)
            return res.status(404).json({ error: "Session doesnt exist." });

        if (session.agent_id !== agentId) {
            return res.status(403).json({ error: 'Session doesnt belong to your agent.' });
        }

        //If its ticket cant be done again
        if (Boolean(session.is_ticket))
            return res.json({ success: true })

        //Collect messages from thread
        const messagesResponse = await listMessagesOnThread(session.thread_id);

        const messages = messagesResponse.data.reverse();

        //Store each message locally
        for (let index = 0; index < messages.length; index++) {
            const message = messages[index];
            await Message.addMessageToTicket(message.role, message.content[0].text.value, sessionId)
        }

        //Register session as ticket
        const response = await Session.assignAsTicket(sessionId, 1);
        return res.json({ success: true, data: response })
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * ADD MESSAGE TO TICKET
 * @param {*} req.params.sId Ticket ID
 * @returns success state
 */
const addMessageToTicket = async (req, res) => {
    try {
        const sessionId = Number(req.params.id);
        const { role, content } = req.body

        //Get ticket
        const session = await Session.getOne(sessionId);

        //Verify session
        if (!session)
            return res.status(404).json({ error: "Session doesnt exist." });

        if (!Boolean(session.is_ticket))
            return res.json({ success: false, error: "Session is not ticket." })

        //Register new message
        await Message.addMessageToTicket(role, content, sessionId)

        return res.json({ success: true })
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

/**
 * SET NEW TUNE STATUS TO SESSION
 * @param {*} req.params.id Agent ID
 * @param {*} req.params.sId Session ID
 * @param {*} req.body.tuneStatus New status
 * @returns update status
 */
const setTuneStatus = async (req, res) => {
    try {
        const agentId = Number(req.params.id);
        const sessionId = Number(req.params.sId);
        const tuneStatus = Number(req.body.tuneStatus);

        //Get session
        const session = await Session.getOne(sessionId);

        //Verify session
        if (!session)
            return res.status(404).json({ error: "Session doesnt exist." });

        if (session.agent_id !== agentId) {
            return res.status(403).json({ error: 'Session doesnt belong to your agent.' });
        }

        //Update session
        const response = await Session.setTuneStatus(sessionId, tuneStatus);
        return res.json(response)
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

module.exports = {
    getSessionsForAgent,
    addSessionToAgent,
    getSessionData,
    updateSessionTags,
    createSession,
    removeSession,
    assignAsTicket,
    setTuneStatus,
    getTicketsForAgent,
    getScheduledForAgent,
    addMessageToTicket
}