const User = require('../models/user')
const { hashPassword, comparePassword } = require('../helpers/auth');
const { generatePassword } = require('../helpers/pwGenerator');
const { sendMail } = require('../integrations/emailSender');
const { registerManagerSubject, registerManagerBody } = require('../helpers/emailMessages');

/**
 * LIST ALL COMPANY MANAGERS
 * @param {*} req.company Company ID provided by the middleware
 * @returns List of managers
 */
const getManagersForCompany = async (req, res) => {
    try {
        const companyId = Number(req.company);
        const user = await User.getAll(companyId);
        res.json(user);
    } catch (error) {
        res.status(404).json({ error: 'Company ID not found.' })
    }

}

/**
 * UPDATE PASSWORD
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.params.id Managers ID
 * @param {*} req.body {password:"", newPassword: ""}
 * @returns Update result or 404 if manager was not found or 401 if password doesn't match
 */
const updatePassword = async (req, res) => {
    try {
        const managerId = Number(req.params.id);
        const rawPassword = req.body

        //Control managers existance
        const manager = await User.getOne(managerId);

        if (!manager)
            return res.status(404).json({ success: false, error: "Manager not found." });

        //Check whether the user knows the existing password
        if (await comparePassword(rawPassword.password, manager.password))
            return res.status(401).json({ success: false, error: "Old passwords dont match." })

        //Hash new Pw
        const newPassword = await hashPassword(rawPassword.newPassword);

        //Update pw
        const result = await User.updatePassword(managerId, newPassword);

        return res.json(result)

    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}

/**
 * DELETE MANAGER ACCOUNT
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.params.id Manager ID
 * @returns success state or 403 if not enough permission
 */
const deleteManager = async (req, res) => {
    const companyId = Number(req.company)
    const managerId = Number(req.params.id)

    //Control manager existance
    const user = await User.getOne(managerId)
    if (!user || !user.id)
        return res.json({ success: true })

    //Validate matching company
    if (user.company_id !== companyId)
        return res.status(403).json({ error: "The manager doesnt belong to your company." })

    //Validate permissions - cant delete upward
    if (user.is_admin || user.is_supervisor)
        return res.status(403).json({ error: "The account has higher status thus cant be deleted." })

    //Delete user
    const deleted = await User.deleteOne(managerId)

    return res.json(deleted)
}

//Register company - LEGACY
const registerSupervisor = async (req, res) => {
    req.body.usertype = "Supervisor";
    return registerUser(req, res);
}

/**
 * REGISTER NEW MANAGER
 * @param {*} req.company Company ID provided by the middleware
 * @param {*} req.body.name Managers name
 * @param {*} req.body.email Managers email
 * @returns success status and error message
 */
const registerManager = async (req, res) => {
    try {
        const companyId = Number(req.company);
        const { name, email } = req.body

        //Check whether all params are entered
        if (!name || !email)
            return res.json({ succes: false, error: 'Name and email is required.' })

        //Check existance
        const exist = await User.findOne(email);
        if (exist.length) {
            return res.json({ succes: false, error: 'Email is already registered.' })
        };

        //Generate and hash new password
        const password = generatePassword(8);
        const hashedPassword = await hashPassword(password);

        //Register user
        const user = await User.create(name, email, hashedPassword, 0, 0, 1, companyId);

        //Send access for the new manager via email
        if (user && user.insertId) {
            const email = sendMail(
                req.body.email,
                registerManagerSubject(),
                registerManagerBody(email, password)
            )
        }

        return res.json({ success: true })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }
}


//Register endpoint - LEGACY
const registerUser = async (req) => {
    try {
        const { name, email, password, usertype, company } = req.body;
        const companyId = Number(company);

        if (!companyId) {
            return {
                error: 'Please register under a company ID.'
            }
        }

        const exist = await User.findOne(email);
        // check if email was entered
        if (exist.length) {
            return {
                error: 'Email is taken alrady.'
            }
        };

        let isManager = 1;
        let isSupervisor = 0;
        let isAdmin = 0;
        if (usertype === "Manager")
            isManager = 1;
        if (usertype === "Supervisor")
            isSupervisor = 1;
        if (usertype === "Admin")
            isAdmin = 1;

        const hashedPassword = await hashPassword(password);
        const user = await User.create(name, email, hashedPassword, isAdmin, isSupervisor, isManager, companyId);
        return user; //res.json(user)
    } catch (error) {
        console.log(error);
        return { error: "Could not register user." }
    }
}

module.exports = {
    registerUser,
    registerSupervisor,
    registerManager,
    getManagersForCompany,
    deleteManager,
    updatePasword: updatePassword
}