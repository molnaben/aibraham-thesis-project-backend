const bcrypt = require('bcrypt')

/**
 * HASH PASSWORD USING BCRYPT
 * @param {*} password Users password
 * @returns password promise
 */
const hashPassword = (password) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(12, (err, salt) => {
            if(err){
                reject(err)
            }
            bcrypt.hash(password, salt, (err, hash)=>{
                if(err){
                    reject(err)
                }
                resolve(hash)
            })
        })
    })
}

/**
 * COMPARE PASSWORD WITH PASSWORD HASH
 * @param {*} password Users password
 * @param {*} hashed Potential password of the user account
 * @returns 
 */
const comparePassword = async (password, hashed) => {
    return bcrypt.compare(password, hashed)
}

module.exports = {
    hashPassword,
    comparePassword
}