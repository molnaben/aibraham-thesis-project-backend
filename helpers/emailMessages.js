/**
 *  THIS FILE CONTAINS ALL THE EMAIL SUBJECTS AND MESSAGES
 */


/* REGISTER MANAGER */
const registerManagerSubject = () => {return "Aibraham - New manager"}
const registerManagerBody = (email, password) => {return `Greetings warrior!\nYou've been assigned as an \"Agent Manager\".\n\nAccount details:\nLogin: ${email}\nPassword: ${password}\n\nEnjoy!\nTeam Aibraham`}

/* PASSWORD RECOVERY */
const forgotPasswordSubject = () => {return "Aibraham - Reset password"}
const forgotPasswordBody = (code) => { return `Hey there!\nSeems like you forgot your password.\nUse this code to reset it:\n\n${code}\n\nWish you the best,\nTeam Aibrahaam`}

module.exports = {
    registerManagerSubject,
    registerManagerBody,
    forgotPasswordBody,
    forgotPasswordSubject

}