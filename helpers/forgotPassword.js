const { sendMail } = require("../integrations/emailSender");
const { findOne, updatePassword } = require("../models/user");
const { hashPassword } = require("./auth");
const { forgotPasswordSubject, forgotPasswordBody } = require("./emailMessages");

//Hold reset tokens
const resetTokens = {};

/**
 * Function to generate a random 6-digit numeric code
 * @returns reset code
 */
const generateRandomCode = () => {
    let code = '';
    for (let i = 0; i < 6; i++) {
        code += Math.floor(Math.random() * 10); // Generate a random digit (0-9)
    }
    return code;
}

/**
 * Remove outdated reset codes
 */
const removeOutdatedResetCodes = () => {
    const currentTime = Date.now();
    const expiryTime = 3600000; // 1 hour

    Object.keys(resetTokens).forEach(resetCode => {
        if (currentTime - resetTokens[resetCode].timestamp > expiryTime) {
            delete resetTokens[resetCode];
            console.log(`Reset code ${resetCode} removed (expired).`);
        }
    });
}

// Call the function periodically (every hour) to remove outdated tokens
setInterval(removeOutdatedResetCodes, 3600000);

/**
 * RESET PASSWORD - COLLECT EMAIL AND SEND RESET TOKEN VIA MAIL
 * @param {*} req.body.email Users email
 * @returns success - all the time
 */
const resetPassword = async (req, res) => {
    try {
        const { email } = req.body

        //Find user
        const user = await findOne(email);

        if (!user || !user.id) {
            console.log("Password recovery - not existing user.")
            return res.json({ success: true }); //no feedback even if the account doesnt exist
        }

        //Generate reset code
        const resetCode = generateRandomCode();

        //Register reset code
        resetTokens[resetCode] = { timestamp: Date.now(), userId: user.id };

        //Send reset code in mail
        await sendMail(email, forgotPasswordSubject(), forgotPasswordBody(resetCode));

        return res.json({ success: true });
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }

};

// Endpoint to reset password with the reset code
/**
 * RESET PASSWORD WITH THE RESET TOKEN
 * @param {*} req.body.resetCode Reset token sent to the user via mail
 * @param {*} req.body.newPassword New password from the user
 * @returns 400 or success code
 */
const submitResetPassword = async (req, res) => {

    try {
        const { resetCode, newPassword } = req.body

        // Check if the reset code exists and is valid
        if (resetTokens[resetCode]) {
            if (Date.now() - resetTokens[resetCode].timestamp <= 3600000) {

                //Hash the new password
                const newPasswordCrypted = await hashPassword(newPassword);

                //Update the password
                const result = await updatePassword(resetTokens[resetCode].userId, newPasswordCrypted);

                res.json(result);
                delete resetTokens[resetCode];

                return;
            } else {
                delete resetTokens[resetCode];
                res.status(400).json({ success: false, error: 'Expired reset code.' });
            }
        } else {
            res.status(400).json({ success: false, error: 'Invalid reset code.' });
        }
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error: 'Internal error.' })
    }


};

module.exports = {
    resetPassword,
    submitResetPassword
}