
/**
 * GENERATE RANDOM PASSWORD
 * @param {*} length Length of the passwordd
 * @returns random char password of length x
 */
function generatePassword(length) {
    const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let password = '';
    for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * charset.length);
        password += charset[randomIndex];
    }
    return password;
}

module.exports={
    generatePassword
}