const crypto = require('crypto');

/**
 * GENERATE SECRET KEY
 * @param {*} length Length of the key, default = 16
 * @param {*} encoding Encoding of the key, default = base54
 * @returns Unique secret key
 */
function generateSecretKey(length = 16, encoding = 'base64') {
  // Generate random bytes
  const randomBytes = crypto.randomBytes(length);
  // Encode bytes to string
  const secretKey = randomBytes.toString(encoding);
  return secretKey;
}


module.exports = {
    generateSecretKey
}