const shortid = require('shortid');

/**
 * GENERATE UNIQUE SHORT ID
 * @returns Short ID
 */
const getUniqueid = () => {
    return shortid.generate();
}

module.exports={
    getUniqueid
}