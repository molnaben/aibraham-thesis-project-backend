const express = require('express');
const dotenv = require('dotenv').config();
const cors = require('cors');
const cookieParser = require('cookie-parser')

const app = express();

function requestLogger(req, res, next) {
    const { method, url } = req;
    console.log(`[${new Date().toISOString()}] ${method} ${url}`);
    next();
  }


/* MIDDLEWARES */

//app.use(requestLogger);
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({extended: false}))


//Readd routes
app.use('/api', require('./routes/authRoutes'));

const port = 3000;
app.listen(port, () => console.log(`Server is running on port ${port}`))