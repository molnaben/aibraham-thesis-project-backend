const MailListener = require('mail-listener2');

//Array keeping the downloaded emails
const emailQueue = [];

// Create a new instance of MailListener
const mailListener = new MailListener({
    username: process.env.EMAIL_ADDRESS,
    password: process.env.EMAIL_PASSWORD,
    host: 'email.active24.com', //Change provider
    port: 993, //Change port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'INBOX',
    markSeen: true // Mark emails as read after fetching
});

// Start listening for incoming emails
//mailListener.start();

// Event listener for new emails
mailListener.on('mail', function(mail){
    //console.log('New email received:');
    //console.log('From:', mail.from);
    //console.log('Subject:', mail.subject);
    //console.log('Body:', mail.text); // If the email is plain text
    //console.log('HTML Body:', mail.html); // If the email includes HTML content

    //Push mail to the queue
    emailQueue.push({
        sender: mail.from[0].address,
        subject: mail.subject,
        body: mail.text
    });
});

// Event listener for errors
mailListener.on('error', function(err){
    console.error('Mail listener error:', err);
});

module.exports = {
    mailListener,
    emailQueue
}