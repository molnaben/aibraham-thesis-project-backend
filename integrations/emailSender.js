const nodeMailer = require('nodemailer')

//EMAIL TEMPLATE EXAMPLE
const messageExample = {
    from: '"Aibraham" <aibraham@clockwork.sk>',
    to: "email@email.sk",
    subject: "Sample subject",
    text: "Text body"
}

//SET UP SMTP CONNECTION, DETAILS DEFINED IN THE .ENV, ACTIVATES ON STARTUP
//SECURE - FALSE for TLS
const transporter = nodeMailer.createTransport({
    host: process.env.SMTP_SERVER,
    port: Number(process.env.SMTP_PORT),
    secure: false,
    auth:{
        user: process.env.EMAIL_ADDRESS,
        pass: process.env.EMAIL_PASSWORD
    }
})

/**
 * SEND EMAIL
 * 
 * @param {string} toAddress - Email will be sent to this address
 * @param {string} subject - Email will have this subject
 * @param {string} body - Email will contain this body
 * @returns messageId in case of success, {error:""} in case of fail
 */
const sendMail = async (toAddress, subject, body ) => {

    const message = {
        from: `"${process.env.EMAIL_NAME}" <${process.env.EMAIL_ADDRESS}>`,
        to: `${toAddress}`,
        subject: `${subject}`,
        text: `${body}`
    }

    try {

        const info = await transporter.sendMail(message);
        return info.messageId;

    } catch (error) {

        console.log(error);
        return {error:"The email could not be sent."}

    }
}

module.exports = {
    sendMail
}