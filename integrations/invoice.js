const axios = require('axios');

/**
 * CREATE AN INVOICE VIA SUPERFAKTURA
 * @param {*} invoiceName Creators company ID
 * @param {*} items List of items in the form of: {"name": "Item1", "unit_price": 3}
 * @param {*} clientName Receivers name
 * @param {*} clientIco Receivers business ID
 * @param {*} clientVat Receivers VAT ID
 * @returns Created invoice ID, invoice token, total amount
 */
const createInvoice = async (invoiceName, items, clientName, clientIco, clientVat) => {

    //Set up the accepted data model
    const data = {
        "Invoice": {
            "name": invoiceName
        },
        "InvoiceItem": items,
        "Client": {
            "name": clientName,
            "ico": clientIco,
            "dic": clientVat
        }
    };

    try {
        //Post the object to create the invoice with the valid credentials
        const response = await axios.post(`${process.env.SUPERFAKTURA_BASE_URL}/invoices/create`, data, {
            headers: {
                'Authorization': `SFAPI email=${process.env.SUPERFAKTURA_EMAIL}&apikey=${process.env.SUPERFAKTURA_API}&company_id=${process.env.SUPERFAKTURA_COMPANY_ID}`
            }
        });

        console.log('Invoice created:', response.data);

        return { success: true, id: response.data.data.Invoice.id, token: response.data.data.Invoice.token, amount: response.data.data.Invoice.amount }
    } catch (error) {
        console.error('Error creating invoice:', error);
        return { success: false }
    }
}

/**
 * ACCESS AND DOWNLOAD THE INVOICE FROM SUPERFAKUTRA - PDF
 * @param {String} req.params.id Invoice ID
 * @param {String} req.params.token Invoice token
 * @returns PDF in the response
 */
async function downloadInvoicePDF(req, res) {

    const { id, token } = req.params

    try {
        //Fetch the invoice with the proper credentials
        const response = await axios.get(`${process.env.SUPERFAKTURA_BASE_URL}/slo/invoices/pdf/${id}/token:${token}`, {
            headers: {
                'Authorization': `SFAPI email=${process.env.SUPERFAKTURA_EMAIL}&apikey=${process.env.SUPERFAKTURA_API}&company_id=${process.env.SUPERFAKTURA_COMPANY_ID}`
            },
            responseType: 'stream'
        });

        //Set response to be PDF and downloadable
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename="invoice.pdf"');

        return response.data.pipe(res);

    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

module.exports = {
    createInvoice,
    downloadInvoicePDF
}
