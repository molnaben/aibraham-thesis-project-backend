//Source: OpenAI documentation

const OpenAI = require("openai");

const openai = new OpenAI({ apiKey: process.env.OPENAI_API_KEY }); // Set up with api key
const MODEL = "gpt-3.5-turbo-0125" // Define the model

/**
 * CREATE AN OPENAI ASSISTANT
 * @param {*} name Assistants name
 * @param {*} instructions Default instructions
 * @returns Assistant object:
 * {
  "id": "asst_abc123",
  "object": "assistant",
  "created_at": 1698984975,
  "name": "Math Tutor",
  "description": null,
  "model": "gpt-4",
  "instructions": "You are a personal math tutor. When asked a question, write and run Python code to answer the question.",
  "tools": [
    {
      "type": "code_interpreter"
    }
  ],
  "file_ids": [],
  "metadata": {}
  }
 */
const createAssistant = async (name, instructions) => {
  const assistant = await openai.beta.assistants.create({
    name: name,
    instructions: instructions,
    tools: [],
    model: MODEL
  });
  return assistant;
}

/**
 * UPDATE AN EXISTING ASSISTANT
 * @param {*} assistantId Assistants ID
 * @param {*} instructions Default instructions
 * @param {*} tools Default tools
 * @param {*} name Assistants name
 * @returns Assistants object - See createAssistant function
 */
const updateAssistant = async (assistantId, instructions, tools, name) => {

  body = {}

  if (instructions) body.instructions = instructions;
  if (name) body.name = name;
  if (tools.length) body.tools = tools;

  const updatedAssistant = await openai.beta.assistants.update(
    assistantId,
    body
  );

  return updatedAssistant;
}

/**
 * CREATE CONVERSATION THREAD
 * @returns Thread object:
 * {
    "id": "thread_abc123",
    "object": "thread",
    "created_at": 1699012949,
    "metadata": {}
    }
 */
const createThread = async () => {
  const thread = await openai.beta.threads.create();
  return thread;
}

/**
 * RUN THREAD - LET THE AGENT ANSWER IT
 * @param {*} threadId Threads ID
 * @param {*} assistantId Assistants ID - who will generate the answer
 * @returns run object - see function retrieveRun - the status of the run needs to be checked
 */
const runThread = async (threadId, assistantId) => {

  const run = await openai.beta.threads.runs.create(
    threadId,
    { assistant_id: assistantId }
  );

  return run;
}

/**
 * CHECK THE STATUS OF THE RUN
 * Needs to check the status for the run - if completed need to retrieve the messages
 * @param {*} threadId Threads ID
 * @param {*} runId Runs ID
 * @returns Run object:
 * {
  "id": "run_abc123",
  "object": "thread.run",
  "created_at": 1699075072,
  "assistant_id": "asst_abc123",
  "thread_id": "thread_abc123",
  "status": "completed",
  "started_at": 1699075072,
  "expires_at": null,
  "cancelled_at": null,
  "failed_at": null,
  "completed_at": 1699075073,
  "last_error": null,
  "model": "gpt-3.5-turbo",
  "instructions": null,
  "tools": [
    {
      "type": "code_interpreter"
    }
  ],
  "file_ids": [
    "file-abc123",
    "file-abc456"
  ],
  "metadata": {},
  "usage": {
    "prompt_tokens": 123,
    "completion_tokens": 456,
    "total_tokens": 579
  }
}
 */
const retrieveRun = async (threadId, runId) => {

  const run = await openai.beta.threads.runs.retrieve(
    threadId,
    runId
  );

  return run;
}


/**
 * ADD A MESSAGE TO A THREAD
 * @param {*} threadId Threads ID
 * @param {*} role Messagers sender role - only allowed "user"
 * @param {*} content Message
 * @returns Message object:
 * {
  "id": "msg_abc123",
  "object": "thread.message",
  "created_at": 1699017614,
  "thread_id": "thread_abc123",
  "role": "user",
  "content": [
    {
      "type": "text",
      "text": {
        "value": "How does AI work? Explain it in simple terms.",
        "annotations": []
      }
    }
  ],
  "file_ids": [],
  "assistant_id": null,
  "run_id": null,
  "metadata": {}
}s
 */
const addMessageToThread = async (threadId, role, content) => {

  const newMessage = await openai.beta.threads.messages.create(
    threadId,
    //role: user / assistant
    { role: role, content: content }
  );

  return newMessage
}

/**
 * FETCH ALL MESSAGES ON THE THREAD
 * @param {*} threadId Threads ID
 * https://platform.openai.com/docs/api-reference/messages/listMessages
 * accepts limit and paging
 */
const listMessagesOnThread = async (threadId) => {

  const threadMessages = await openai.beta.threads.messages.list(
    threadId,
    {limit: 100}
  );

  return threadMessages;
}

/**
 * SUBMIT THE OUTPUT OF EXTERNAL FUNCTION
 * @param {*} threadId Threads ID
 * @param {*} runId Run ID
 * @param {*} output The output of the function
 * {
    tool_call_id: "call_abc123",
    output: "28C",
    }
 * @returns Run object - see retrieveRun function
 */
const submitToolOutput = async (threadId, runId, output) => {
  const run = await openai.beta.threads.runs.submitToolOutputs(
    threadId,
    runId,
    {
      tool_outputs: output,
    }
  );
  return run;
}

/**
 * RETURNS THE LAST RUN OF ALL ON THREAD
 * @param {*} thread Thread ID
 * @returns Run object - see retrieveRun function
 */
const returnLastRun = async (thread) => {

  const runs = await openai.beta.threads.runs.list(
    thread
  );

  return runs.data[0]
}

module.exports = {
  createAssistant,
  createThread,
  addMessageToThread,
  listMessagesOnThread,
  runThread,
  retrieveRun,
  updateAssistant,
  returnLastRun,
  submitToolOutput
}