const { getAgent } = require("../models/agent");
const { findOneById } = require("../models/company");
const { getOne } = require("../models/session");

/**
 * CHECKS IF A SESSION IS RESTRICTED OR NOT, BASED ON SESSION ID
 * @param {String} req.params.id Sessions internal ID
 * @returns 404 if the session is not found - or passes to next middleware
 */
const controlRestrictionsMessage = async (req, res, next) => {
    try {
        const requestSession = req.params.id;

        if(!requestSession)
            return res.status(404).json({ error: 'No session key found.' });

        const session = await getOne(requestSession);

        if(!session || !session.id_session)
            return res.status(404).json({ error: 'No session found.' });

        const result = await checkRestrictions(res, session.agent_id);

        if(!result)
            return;

        next();
    } catch (error) {
        console.log(error);
        return res.status(500)
    }
}

/**
 * CHECKS IF A SESSION IS RESTRICTED OR NOT, BASED ON AGENT ID
 * @param {String} req.body.agentKey Agents internal ID
 * @returns 404 if the session is not found - or passes to next middleware
 */
const controlRestrictionsSession = async (req, res, next) => {
    try {
        const requestAgent = req.body.agentKey;

        if(!requestAgent)
            return res.status(404).json({ error: 'No agent key found.' });

        const result = await checkRestrictions(res, requestAgent);

        if(!result)
            return;

        next();
    } catch (error) {
        console.log(error);
        return res.status(500)
    }
}

/**
 * CHECK INPUT AGENT ON ANY RESTRICTION, RETURN PROPER RESPONSE
 * @param {*} res Uses to send back the proper response
 * @param {Number} agentId Agents internal ID
 * @returns error message, or continues to the next middleware
 */
const checkRestrictions = async (res, agentId) => {
    
    //Check if agent is offline

    const agent = await getAgent(agentId);

    if(!agent || !agent.id_agent){
        res.status(404).json({ error: 'No agent found.' })
        return false;
    }

    if(!agent.is_active){
        res.json({success:false, error: Boolean(agent.is_custom_error) ? agent.error_message : 'Agent is offline.'})
        return false;
    }
        
    //Check if controlling company is disabled

    const company = await findOneById(agent.company_id)

    if(!company || !company.id){
        res.status(404).json({ error: 'No company found.' })
        return false;
    }

    if(!company.is_active){
        res.json({success:false, error: Boolean(agent.is_custom_error) ? agent.error_message : 'Company is disabled.'})
        return false;
    }

    return true;
        
}

module.exports={
    controlRestrictionsMessage,
    controlRestrictionsSession
}