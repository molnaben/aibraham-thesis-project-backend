const { getOneBySecret } = require("../models/agent");
const {  getOneByPublic } = require("../models/session");

/**
 * TRANSLATES THE SESSIONS PUBLIC ID TO INTERNAL ID
 * @param {String} req.params.id Sessions Public ID
 * @returns 404 if the session is not found - or passes to next middleware
 */
const parsePublicSession = async (req, res, next) => {
    try {
        const requestSession = req.params.id

        if(!requestSession)
            return res.status(404).json({ error: 'No session key found.' })

        const session = await getOneByPublic(requestSession);

        if(!session || !session.id_session)
            return res.status(404).json({ error: 'No session found.' })

        req.params.id = session.id_session;

        next()
    } catch (error) {
        console.log(error);
        return res.status(500)
    }
}

/**
 * TRANSLATES THE AGENTS SECRET KEY TO INTERNAL ID
 * @param {String} req.body.agentKey Agents Secret Key
 * @returns 404 if the agent is not found - or passes to next middleware
 */
const parsePublicAgentKey = async (req, res, next) => {
    try {
        const requestAgent = req.body.agentKey

        if(!requestAgent)
            return res.status(404).json({ error: 'No agent key found.' })

        const agent = await getOneBySecret(requestAgent);

        if(!agent || !agent.id_agent)
            return res.status(404).json({ error: 'No agent found.' })

        req.body.agentKey = agent.id_agent;

        next()
    } catch (error) {
        console.log(error);
        return res.status(500)
    }
}

module.exports = {
    parsePublicSession,
    parsePublicAgentKey
}