const Agent = require('../models/agent')

/**
 * VERIFY WHETHER THE AGENT BELONGS TO THE REQUESTING COMPANY
 * @param {String} req.params.id Agent ID
 * @param {Number} req.company Company ID - comping from the verifyToken middleware
 * @returns 404 if agent is not found, 403 if the agent doesn't belong to the agents company - or passes to next middleware
 */
const verifyAgentCompany = async (req, res, next) => {

    try {
        const requestAgent = Number(req.params.id)
        const requestCompany = Number(req.company);

        if(!requestAgent)
            return res.status(404).json({ error: 'No agent found.' })

        const origSettings = await Agent.getAgent(requestAgent);

        if (Number(origSettings.company_id) !== requestCompany) {
            return res.status(403).json({ error: 'This agent doesnt belong to you.' });
        }

        next()
    } catch (error) {
        console.log(error);
        return res.status(500)
    }
    
}

module.exports = verifyAgentCompany