
/**
 * CONTROL USER ROLES
 * @param  {Array} allowedRoles - define roles on call which are allowed to access the endpoint
 * @returns 401 or passes through to the next middleware
 */
const verifyRoles = (...allowedRoles) => {
    return (req,res,next) => {
        
        if(!req?.roles) return res.sendStatus(401);

        const rolesArray = [...allowedRoles];

        const result = req.roles.map(role => rolesArray.includes(role)).find(value => value === true);
        
        if(!result) return res.sendStatus(401);

        next();
    }
}

module.exports = verifyRoles