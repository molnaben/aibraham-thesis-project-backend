const jwt = require('jsonwebtoken')

/**
 * VERIFY TOKEN MIDDLEWARE - RETURNS USER DATA IN THE REQUEST BASED ON THE TOKEN
 * @returns 401 if there is no _auth token or passes through to the next middleware
 */
const verifyJWT = (req, res, next) => {
    const {_auth} = req.cookies

    if(!_auth){
        return res.sendStatus(401);
    }

    jwt.verify(_auth, process.env.JWT_SECRET, {}, (err, user) => {
        if(err) return res.sendStatus(403);
        req.user = user.name;
        req.id = user.id;
        req.email = user.email;
        req.roles = user.roles;
        req.company = user.company
        next();
    } )
}

module.exports = verifyJWT