const database = require('../controllers/databaseController');

const getAll = async (company_id) => {
    const [result] = await database.query(`SELECT * FROM agent WHERE company_id = ?`, company_id);
    return result
}

const getAgent = async (id_agent) => {
    const [result] = await database.query(`SELECT * FROM agent WHERE id_agent = ?`, id_agent);
    return result[0]
}

const getOneBySecret = async (secret_key) => {
    const [result] = await database.query(`SELECT * FROM agent WHERE secret_key = ?`, secret_key);
    return result[0]
}

const updateAgent = async (id_agent, name, company_id, is_active, is_custom_error, error_message, accept_email) => {
    const [result] = await database.query(`
        UPDATE agent 
        SET name = ?, company_id = ?, is_active = ?, is_custom_error = ?, error_message = ?, accept_email = ?
        WHERE id_agent = ?
    `, [name, company_id, is_active, is_custom_error, error_message, accept_email, id_agent]);

    return result;
}

const createAgent = async (name, company_id, is_active, is_custom_error, error_message, accept_email, assistant_id, type, secret_key) => {
    const [result] = await database.query(`
    INSERT INTO agent (name, company_id, is_active, is_custom_error, error_message, accept_email, assistant_id, type, secret_key)
    VALUES (?, ?, ?, ?, ?, ? , ?, ?, ?)
    `, [name, company_id, is_active, is_custom_error, error_message, accept_email, assistant_id, type, secret_key]);

    return result;
}

module.exports = {
    getAll,
    getAgent,
    updateAgent,
    createAgent,
    getOneBySecret
}