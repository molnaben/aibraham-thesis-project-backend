//DEFAULT AGENT FUNCTIONS



const AGENT_DEFAULT_FUNCTIONS = [

  //Migrate agent session to ticket
  {
    type: "function",
    function: {
      name: "migrate_current_chat",
      description: "If the client is interested and ready to finish the deal, or needs information you cant provide let our human colleague handle it.",
      parameters: {},
    },
  },
]

module.exports = AGENT_DEFAULT_FUNCTIONS