const database = require('../controllers/databaseController');

const createBillForCompany = async (bill_number, token, is_paid, company_id, amount) => {
    const [result] = await database.query(`
    INSERT INTO bill (bill_number, token, is_paid, creation_date, due_date, company_id, amount)
    VALUES (?, ?, ?, CURRENT_TIMESTAMP, DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 14 DAY), ?, ?)
    `, [bill_number, token, is_paid, company_id, amount]);

    return result;
}

const getAllForCompany = async (company_id) => {
    const [result] = await database.query(`SELECT * FROM bill WHERE company_id = ?`, company_id);
    return result
}

const getBills = async () => {
    const [bills] = await database.query(`SELECT * FROM bill`);
    return bills
}

const markPaid = async (id) => {
    const [result] = await database.query(`
        UPDATE bill 
        SET is_paid = 1
        WHERE bill_number = ?
    `, [id]);

    return result;
}

module.exports = {
    createBillForCompany,
    getAllForCompany,
    getBills,
    markPaid
}