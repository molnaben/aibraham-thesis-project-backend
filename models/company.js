const database = require('../controllers/databaseController');
const User = require('./user')

const getCompanies = async () => {
    const [companies] = await database.query("SELECT * FROM company");
    return companies;
}

const findOneById = async (id) => {
    const [company] = await database.query("SELECT * FROM company WHERE id = ?", [id]);
    return company[0];
}

const findOneByBizId = async (id) => {
    const company = await database.query("SELECT * FROM company WHERE business_id = ?", [id]);
    return company[0];
}

const create = async (name, is_business, business_id, vat_id, is_active, monthly_cost) => {
    const [result] = await database.query(`
    INSERT INTO company (name, is_business, business_id, vat_id, is_active, monthly_cost)
    VALUES (?, ?, ?, ?, ?, ?)
    `, [name, is_business, business_id, vat_id, is_active, monthly_cost]);

    return result;
}

const update = async (id, name, business_id, vat_id, is_active, monthly_cost) => {
    const [result] = await database.query(`
        UPDATE company 
        SET name = ?, is_business = ?, business_id = ?, vat_id = ?, is_active = ?, monthly_cost = ?
        WHERE id = ?
    `, [name, is_business, business_id, vat_id, is_active, monthly_cost, id]);

    return result;
}

const addToCost = async (companyId, additionalCost) => {
    let connection;
    try {
        connection = await database.getConnection();
        // Begin transaction
        await connection.beginTransaction();
    
        // Fetch the current monthly cost
        const [rows] = await connection.query('SELECT * FROM company WHERE id = ?', [companyId]);
        const currentMonthlyCost = rows[0].monthly_cost;
    
        // Calculate the new monthly cost
        const newMonthlyCost = currentMonthlyCost + additionalCost;
    
        // Update the monthly cost
        await connection.query('UPDATE company SET monthly_cost = ? WHERE id = ?', [newMonthlyCost, companyId]);
    
        // Commit transaction
        await connection.commit();
    
        console.log('Monthly cost updated successfully');
        return {success:true}
      } catch (error) {
        // Rollback transaction on error
        if (connection) {
          await connection.rollback();
        }
        console.error('Error updating monthly cost:', error);
        return {success:false}
    } finally {
        // Release the connection
        if (connection) {
          connection.release();
        }
    }
}

//Adding time to membership validity
const addToMembership = async (companyId, addedDays) => {
    let connection;
    try {
        connection = await database.getConnection();
        // Begin transaction
        await connection.beginTransaction();
    
        const [rows] = await connection.query('SELECT * FROM company WHERE id = ?', [companyId]);
        
        const currentDate = new Date();

        // If it ended in the past start it from current date.. if it ends in the future add to that
        const newEndDate = new Date(rows[0].membership_end < currentDate ? currentDate : rows[0].membership_end);
        newEndDate.setDate(newEndDate.getDate() + addedDays);
    
        // Update
        await connection.query('UPDATE company SET membership_end = ? WHERE id = ?', [newEndDate, companyId]);
    
        // Commit transaction
        await connection.commit();
    
        console.log('Membership end date updated successfully');
        return {success:true}
      } catch (error) {
        // Rollback transaction on error
        if (connection) {
          await connection.rollback();
        }
        console.error('Error updating membership date:', error);
        return {success:false}
    } finally {
        // Release the connection
        if (connection) {
          connection.release();
        }
    }
}

const resetCost = async (companyId) => {
    try {
        await database.query('UPDATE company SET monthly_cost = 0 WHERE id = ?', [companyId]);
        return {success:true}
    } catch (error) {
        return {success:false}
    }
}

const disableCompany = async (companyId) => {
    try {
        await database.query('UPDATE company SET is_active = 0 WHERE id = ?', [companyId]);
        return {success:true}
    } catch (error) {
        return {success:false}
    }
}

const activateCompany = async (companyId) => {
    try {
        await database.query('UPDATE company SET is_active = 1 WHERE id = ?', [companyId]);
        return {success:true}
    } catch (error) {
        return {success:false}
    }
}

//Register a company account with a supervisor account
const register = async (businessName, isBusiness, businessId, vatId, name, email, password) => {
    let connection;
    try {

        connection = await database.getConnection();

        // Begin transaction
        await connection.beginTransaction();
    
        //Register company
        const company = await create(businessName, isBusiness, businessId, vatId, 0, 0);

        //Control creation
        if(!company && !company.insertId)
            throw {success:false, error:"Couldnt create company."}
    
        //Register supervisor for the company
        const user = await User.create(name, email, password, 0, 1, 1, company.insertId)

        //Control creation
        if(!user && !user.insertId)
            throw {success:false, error:"Couldnt create user."}
    
        //Commit
        await connection.commit();
    
        return {success:true}

      } catch (error) {
        // Rollback transaction on error
        if (connection) {
          await connection.rollback();
        }
        console.error('Error registering company:', error);
        return {success:false}
    } finally {
        // Release the connection
        if (connection) {
          connection.release();
        }
    }
}

module.exports = {
    getCompanies,
    findOneById,
    findOneByBizId,
    create,
    update,
    addToCost,
    resetCost,
    disableCompany,
    activateCompany,
    addToMembership,
    register
}