const database = require('../controllers/databaseController');

const getAll = async (agent_id) => {
    const [result] = await database.query(`SELECT * FROM knowledge WHERE agent_id = ?`, agent_id);
    return result
}

const getOne = async (id_knowledge) => {
    const [result] = await database.query(`SELECT * FROM knowledge WHERE id_knowledge = ?`, id_knowledge);
    return result[0]
}

const createKnowledgeForAgent = async (agent_id, title, content, is_active, is_function) => {
    const [result] = await database.query(`
    INSERT INTO knowledge (agent_id, title, content, is_active, is_function)
    VALUES (?, ?, ?, ?, ?)
    `, [agent_id, title, content, is_active, is_function]);

    return result;
}

const updateKnowledge = async (id_knowledge, title, content, is_active, is_function) => {
    const [result] = await database.query(`
    UPDATE knowledge 
    SET title = ?, content = ?, is_active = ?, is_function = ?, last_modified = CURRENT_TIMESTAMP
    WHERE id_knowledge = ?
    `, [title, content, is_active, is_function, id_knowledge]);

    return result;
}

const removeKnowledge = async (id_knowledge) => {
    try {
        await database.query('DELETE FROM knowledge WHERE id_knowledge = ?', [id_knowledge]);
        return {success: true}
    } catch (error) {
        console.log(error)
        return {success: false, error: "Knowledge could not be deleted."}
    }
}

module.exports = {
    getAll,
    createKnowledgeForAgent,
    getOne,
    updateKnowledge,
    removeKnowledge
}