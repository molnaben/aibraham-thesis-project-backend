const database = require('../controllers/databaseController');

const addMessageToTicket = async (sender, message, session_id) => {
    const [result] = await database.query(`
    INSERT INTO message (sender, message, session_id)
    VALUES (?, ?, ?)
    `, [sender, message, session_id]);
    return result;
}

const getMessagesForTicket = async (session_id) => {
    const [result] = await database.query(`SELECT * FROM message WHERE session_id = ?`, session_id);
    return result
}

module.exports = {
    addMessageToTicket,
    getMessagesForTicket
}