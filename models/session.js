const database = require('../controllers/databaseController');

const getAll = async (agent_id) => {
    const [result] = await database.query(`SELECT * FROM session WHERE agent_id = ?`, agent_id);
    return result
}

const getTickets = async (agent_id) => {
    const [result] = await database.query(`SELECT * FROM session WHERE agent_id = ? AND is_ticket = 1`, agent_id);
    return result
}

const getScheduled = async (agent_id) => {
    const [result] = await database.query(`SELECT * FROM session WHERE agent_id = ? AND finetune = 1`, agent_id);
    return result
}

const getOne = async (id_session) => {
    const [result] = await database.query(`SELECT * FROM session WHERE id_session = ?`, id_session);
    return result[0]
}

const getLastSevenDays = async (agent_id) => {
    const [result] = await database.query(`SELECT
        DATE(opened) AS date,
        COUNT(*) AS opened_sessions
    FROM
        session
    WHERE
        agent_id = ? AND
        opened >= CURDATE() - INTERVAL 7 DAY
    GROUP BY
        DATE(opened)
    ORDER BY
        DATE(opened)
`, agent_id);
    return result
}

const getOneByPublic = async (public_key) => {
    const [result] = await database.query(`SELECT * FROM session WHERE public_key = ?`, public_key);
    return result[0]
}

const getOneByThread = async (thread_id) => {
    const [result] = await database.query(`SELECT * FROM session WHERE thread_id = ?`, thread_id);
    return result[0]
}

const createSessionForAgent = async (agent_id, total_cost, is_email, customer_email, tags, is_ticket, thread_id, finetune, public_key) => {
    const [result] = await database.query(`
    INSERT INTO session (agent_id, total_cost, is_email, customer_email, tags, is_ticket, thread_id, last_modified, opened, finetune, public_key)
    VALUES (?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?,?)
    `, [agent_id, total_cost, is_email, customer_email, JSON.stringify(tags), is_ticket, thread_id, finetune, public_key]);

    return result;
}

const updateSession = async (id_session, total_cost, tags, is_ticket, finetune) => {
    const [result] = await database.query(`
        UPDATE session 
        SET finetune = ?, total_cost = ?, tags = ?, is_ticket = ?, last_modified = CURRENT_TIMESTAMP
        WHERE id_session = ?
    `, [finetune, total_cost, tags, is_ticket, id_session]);

    return result;
}

const addToSessionCost = async (id_session, additionalCost) => {
    const [rows] = await database.query('SELECT * FROM session WHERE id_session = ?', [id_session]);
    const currentTotalCost = rows[0].total_cost;
    
    const newCost = currentTotalCost + additionalCost;

    const [result] = await database.query('UPDATE session SET total_cost = ? WHERE id_session = ?', [newCost, id_session]);
    return result;
}

const removeSession = async (id_session) => {
    try {
        await database.query('DELETE FROM session WHERE id_session = ?', [id_session]);
        return {success: true}
    } catch (error) {
        console.log(error)
        return {success: false, error: "Session could not be deleted."}
    }
    
}

const assignAsTicket = async (id_session, ticketMode) => {
    try {
        await database.query('UPDATE session SET is_ticket = ? WHERE id_session = ?', [ticketMode, id_session]);
        return {success: true}
    } catch (error) {
        console.log(error)
        return {success: false, error: "Session could not be ticketed."}
    }
}

const setTuneStatus = async (id_session, tuneMode) => {
    try {
        await database.query('UPDATE session SET finetune = ? WHERE id_session = ?', [tuneMode, id_session]);
        return {success: true}
    } catch (error) {
        console.log(error)
        return {success: false, error: "Session could not be tune modified."}
    }
}

module.exports = {
    getAll,
    getOne,
    createSessionForAgent,
    updateSession,
    addToSessionCost,
    removeSession,
    assignAsTicket,
    setTuneStatus,
    getTickets,
    getScheduled,
    getOneByThread,
    getOneByPublic,
    getLastSevenDays
}