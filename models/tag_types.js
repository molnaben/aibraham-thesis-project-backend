const isOpen = {
    "name": "Open",
    "color": "#4CBB17"
}

const isClosed = {
    "name": "Closed",
    "color": "#88292F"
}

const attention = {
    "name": "Attention",
    "color": "#FFC43D"
}

const interested = {
    "name": "Interested",
    "color": "#5CC8FF"
}


const getList = () => {
    return [isOpen, isClosed, attention, interested]
}

/**
 * GET ALL AVAILABLE TAGS
 * @returns Array of available tags
 */
const fetchTags = (req, res) => {
    try {
        return res.json({success: true, tags: getList()})
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: 'Internal error.' });
    }
}

module.exports = {
    getList,
    interested,
    attention,
    isClosed,
    isOpen,
    fetchTags
}