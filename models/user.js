const database = require('../controllers/databaseController');

const findOne = async (email) => {
    const [user] = await database.query("SELECT * FROM user WHERE email = ?", [email]);
    return user[0];
}

const getOne = async (id) => {
    const [user] = await database.query("SELECT * FROM user WHERE id = ?", [id]);
    return user[0];
}

const deleteOne = async (id) => {
    try {
        await database.query("DELETE FROM user WHERE id = ?", [id]);
        return { success: true, message: `User with ID ${id} deleted successfully.` };
    } catch (error) {
        console.error('Error deleting user:', error);
        return { success: false, message: 'Error deleting user.', error };
    }
};

const create = async (name, email, password, is_admin, is_supervisor, is_manager, company_id) => {
    const [result] = await database.query(`
    INSERT INTO user (name, email, password, is_admin, is_supervisor, is_manager, company_id)
    VALUES (?, ?, ?, ?, ?, ?, ?)
    `, [name, email, password, is_admin, is_supervisor, is_manager, company_id]);

    return result;
}

const getAll = async (company_id) => {
    const [result] = await database.query(`SELECT * FROM user WHERE company_id = ?`, company_id);
    return result
}

const updatePassword = async (id, newPassword) => {
    try {
        await database.query('UPDATE user SET password = ? WHERE id = ?', [newPassword, id]);
        return {success: true}
    } catch (error) {
        console.log(error)
        return {success: false, error: "Session could not be tune modified."}
    }
}




module.exports = {
    findOne,
    create,
    getAll,
    getOne,
    deleteOne,
    updatePassword
}