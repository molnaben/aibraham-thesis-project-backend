const express = require('express');
const router = express.Router();
const cors = require('cors');
const { loginUser } = require('../controllers/authController')
const { registerCompany } = require('../controllers/companyController')
const ROLES_LIST = require('../models/roles_list')
const verifyRoles = require('../middleware/verifyRoles')
const verifyToken = require('../middleware/verifyToken');
const { registerManager , getManagersForCompany, deleteManager, updatePasword } = require('../controllers/userController');
const { getAgentsForCompany, getAgentDetails, updateAgentSettings, addAgentForCompany, getAgentHome } = require('../controllers/agentController');
const verifyAgentCompany = require('../middleware/verifyAgentCompany');
const { getKnowledgeForAgent, createKnowledgeForAgent, getOneKnowledge, updateKnowledge, deleteKnowledge } = require('../controllers/knowledgeController');
const { getSessionsForAgent, addSessionToAgent, getSessionData, removeSession, assignAsTicket, setTuneStatus, getTicketsForAgent, getScheduledForAgent, updateSessionTags, addMessageToTicket } = require('../controllers/sessionController');
const { mailListener } = require('../integrations/emailReceiver');
const { fetchPublicSession, createPublicSession, addPublicMessage, postPublicFunctionOutput } = require('../controllers/publicController');
const { downloadInvoicePDF } = require('../integrations/invoice');
const { getInvoicesForCompany, getBillingInfo, billOutstanding, payMembership, payInvoice } = require('../controllers/invoiceController');
const { fetchTags } = require('../models/tag_types');
const { parsePublicSession, parsePublicAgentKey } = require('../middleware/parsePublic');
const { controlRestrictionsMessage, controlRestrictionsSession } = require('../middleware/controlRestrictions');
const { resetPassword, submitResetPassword } = require('../helpers/forgotPassword');

mailListener.start();

router.use(
    cors(
        {
        credentials: true,
        origin: ['https://aibraham.tech','https://www.aibraham.tech'] //http://localhost:3000 //
        }
        )
)

router.post('/login', loginUser)
router.post('/login/reset', resetPassword)
router.post('/login/reset/submit', submitResetPassword)
router.post('/register', registerCompany)

router.get('/public/session/tags', fetchTags) // FETCHING AVAILABLE TAGS

router.get('/public/session/:id', [parsePublicSession] ,fetchPublicSession) //fetch session if already exists
router.post('/public/session/:id', [parsePublicSession, controlRestrictionsMessage], addPublicMessage) //add message to a public session
router.post('/public/session', [parsePublicAgentKey, controlRestrictionsSession], createPublicSession) //create a new session, returns session id
router.post('/public/session/call/output', postPublicFunctionOutput) //post output from a called function


router.get('/company/invoices', [verifyToken], getInvoicesForCompany)
router.post('/company/invoice/pay/:id',[verifyToken], payInvoice)
router.get('/company/invoice/:id/download/:token', downloadInvoicePDF)
router.get('/company/billing', [verifyToken], getBillingInfo)

router.post('/company/membership/pay', [verifyToken], payMembership)

router.get('/company/agents', [verifyToken], getAgentsForCompany)
router.post('/company/agents', [verifyToken], addAgentForCompany)
router.get('/company/agent/:id', [verifyToken], getAgentDetails)
router.get('/company/agent/:id/home', [verifyToken], getAgentHome)
router.put('/company/agent/:id', [verifyToken], updateAgentSettings)

router.get('/company/agent/:id/session', [verifyToken, verifyAgentCompany], getSessionsForAgent)
router.post('/company/agent/:id/session', [verifyToken, verifyAgentCompany], addSessionToAgent)
router.get('/company/agent/:id/session/:sId', [verifyToken, verifyAgentCompany], getSessionData)
router.put('/company/agent/:id/session/:sId', [verifyToken, verifyAgentCompany], updateSessionTags)
router.delete('/company/agent/:id/session/:sId', [verifyToken, verifyAgentCompany], removeSession)

router.get('/company/agent/:id/finetune/scheduled', [verifyToken, verifyAgentCompany], getScheduledForAgent)
router.put('/company/agent/:id/session/:sId/finetune', [verifyToken, verifyAgentCompany], setTuneStatus)

router.put('/company/agent/:id/session/:sId/ticket', [verifyToken, verifyAgentCompany], assignAsTicket)
router.get('/company/agent/:id/tickets', [verifyToken, verifyAgentCompany], getTicketsForAgent)
router.post('/company/ticket/:id',[verifyToken], addMessageToTicket)

router.get('/company/agent/:id/knowledge', [verifyToken, verifyAgentCompany], getKnowledgeForAgent)
router.post('/company/agent/:id/knowledge', [verifyToken, verifyAgentCompany], createKnowledgeForAgent)
router.get('/company/agent/:id/knowledge/:kId', [verifyToken, verifyAgentCompany], getOneKnowledge)
router.put('/company/agent/:id/knowledge/:kId', [verifyToken, verifyAgentCompany], updateKnowledge)
router.delete('/company/agent/:id/knowledge/:kId', [verifyToken, verifyAgentCompany], deleteKnowledge)

router.get('/accounts/users', [verifyToken, verifyRoles(ROLES_LIST.Supervisor)], getManagersForCompany )
router.post('/accounts/user', [verifyToken, verifyRoles(ROLES_LIST.Supervisor)], registerManager) //adding managers
router.delete('/accounts/user/:id', [verifyToken, verifyRoles(ROLES_LIST.Supervisor)], deleteManager) //adding managers
//router.put('/accounts/company', [verifyToken, verifyRoles(ROLES_LIST.Supervisor)], updateCompany) //updating company info

module.exports = router